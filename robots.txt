User-agent: *
Disallow: /wp/wp-admin/
Allow: /wp/wp-admin/admin-ajax.php

Sitemap: https://www.sea-hotel.co.il/sitemap_index.xml
Sitemap: https://www.sea-hotel.co.il/he/sitemap_index.xml
Sitemap: https://www.sea-hotel.co.il/es/sitemap_index.xml
Sitemap: https://www.sea-hotel.co.il/fr/sitemap_index.xml
Sitemap: https://www.sea-hotel.co.il/ru/sitemap_index.xml
